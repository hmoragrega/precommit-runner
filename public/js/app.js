
/* Constanst *****************************************************************/
var INFO    = 'info';
var DANGER  = 'danger';
var WARNING = 'warning';
var SUCCESS = 'success';

$.fn.setClass = function(classToSet) {
    this.removeClass(INFO);
    this.removeClass(DANGER);
    this.removeClass(WARNING);
    this.removeClass(SUCCESS);
    this.addClass(classToSet);
};

$.fn.displayErrors = function(errors, warnings) {
    this.find('.errors').text(errors);
    this.find('.warnings').text(warnings);
};

$.fn.clearErrors = function() {
    this.find('.errors').text('');
    this.find('.warnings').text('');
};

$.fn.setStatus = function(label, running) {
    this.html(label);
    this.data('running', running);
};

$.fn.enable = function() {
    this.removeAttr("disabled");
};

$(document).ready(function(){

    /* Runner Class **********************************************************/

    var Runner = function() {
        this.instance = $('select[name=instance]').val();
        this.$modal   = $('#output-modal');

        this.requestOne = function(file) {
            return $.ajax({
                url: 'index.php',
                type: 'post',
                data: {
                    instance: this.instance,
                    file: file
                },
                headers: {
                    // hack to route with silex
                    "X-Original-Url": '/utils/precommit-runner/run'
                },
                dataType: 'json'
            });
        };

        this.completed = function($row, $output, data) {
            $output.data('output', data.output);
            $output.enable();
            if(!data.result) {
                $row.data('class', (data.errors > 0) ? DANGER : WARNING);
                $row.displayErrors(data.errors, data.warnings);
            } else {
                $row.data('class', SUCCESS);
                $row.clearErrors();
            }
        };

        this.failed = function($row, $output, error) {
            if (error.statusText != 'abort') {
                console.log(error);
                $row.data('class', DANGER);
                $output.data('output', 'Error, see console');
                $row.clearErrors();
            }
        };

        this.abort = function($button) {
            $button.data('request').abort();
        };
    };

    Runner.prototype.runOne = function($button){
        var me   = this;
        var file = $button.data('file');
        var $row = $button.closest('tr');
        var $output = $row.find('.view-output');

        $button.setStatus('Cancel', true);
        $row.setClass(INFO);

        $button.data('request',me.requestOne(file)
            .done(function(data) {
                me.completed($row, $output, data);
            }).fail(function(error) {
                me.failed($row, $output, error);
            }).always(function() {
                $row.setClass($row.data('class'));
                $button.setStatus('Run', false);
            })
        );
    };

    Runner.prototype.abortOne = function($button){
        $button.setStatus('Run', false);
        this.abort($button);
    };

    Runner.prototype.runGroup = function($button){
        var me = this;
        $button.setStatus('Cancel', true);
        $button.next('table').find('button.run').each(function(){
            me.runOne($(this));
        });
    };

    Runner.prototype.abortGroup = function($button){
        var me = this;
        $button.setStatus('Run Group', false);
        $button.next('table').find('button.run').each(function(){
            me.abortOne($(this));
        });
    };

    Runner.prototype.runAll = function($button){
        var me = this;
        $button.setStatus('Cancel', true);
        $('button.run-group').each(function(){
            me.runGroup($(this));
        });
    };

    Runner.prototype.abortAll = function($button){
        var me = this;
        $button.setStatus('Run All', false);
        $('button.run-group').each(function(){
            me.abortGroup($(this));
        });
    };

    Runner.prototype.viewOutput = function($button) {
        this.$modal.find('.modal-body').html($button.data('output'));
        this.$modal.modal();
    };

    /* Events *******************************************************/

    $('button.run').on('click', function(){
        if (!$(this).data('running')) {
            runner.runOne($(this));
        } else {
            runner.abortOne($(this));
        }
    });

    $('button.run-group').on('click', function(){
        if (!$(this).data('running')) {
            runner.runGroup($(this));
        } else {
            runner.abortGroup($(this));
        }
    });

    $('button#run-all').on('click', function(){
        if (!$(this).data('running')) {
            runner.runAll($(this));
        } else {
            runner.abortAll($(this));
        }
    });

    $('button.view-output').on('click', function(){
        runner.viewOutput($(this));
    });

    $('select#instance-selector').on('change', function(){
        $(this).closest('form').submit();
    });

    /* The one and only */
    var runner = new Runner();
});