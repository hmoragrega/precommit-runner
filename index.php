<?php

require_once 'vendor/autoload.php';

$runner = new PreCommit\Runner();
PreCommit\Config\WorkingCopy::configure($runner);

$runner->run();