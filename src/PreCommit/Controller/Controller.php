<?php

namespace PreCommit\Controller;

use Twig_Environment;

abstract class Controller
{
    /**
     * @var \Twig_Environment
     */
    protected $view;

    public function __construct(Twig_Environment $view)
    {
        $this->view = $view;
    }
} 