<?php

namespace PreCommit\Controller;

use Twig_Environment;
use PreCommit\Runner;
use Symfony\Component\HttpFoundation\Request;
use PreCommit\Domain\File\Lister as FileLister;
use PreCommit\Domain\Instance\Lister as InstanceLister;

class Home extends Controller
{
    const DEFAULT_INSTANCE = '_root_';
    const DEFAULT_TYPE     = 'subversion';

    /**
     * @var InstanceLister
     */
    protected $instanceLister;

    /**
     * @var CodeLister
     */
    protected $codeLister;

    public function __construct(InstanceLister $instanceLister, FileLister $codeLister, Twig_Environment $view)
    {
        parent::__construct($view);
        $this->instanceLister = $instanceLister;
        $this->codeLister     = $codeLister;
    }

    public function index(Runner $app, Request $request)
    {
        $instance = $request->get('instance') ?: self::DEFAULT_INSTANCE;
        $type     = $request->get('type')  ?: self::DEFAULT_TYPE;

        $variables = array(
            'instances' => $this->instanceLister->getAll(),
            'instance'  => $instance,
            'type'      => $type,
            'files'     => $this->codeLister->getFiles($instance, $type),
        );

        return $this->view->render('home.twig', $variables);
    }
} 