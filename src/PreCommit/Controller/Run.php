<?php

namespace PreCommit\Controller;

use PreCommit\Runner;
use PreCommit\Domain\Executor;
use PreCommit\Domain\Parser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class Run
{
    /**
     * @var \PreCommit\Domain\Executor
     */
    protected $executor;

    /**
     * @var \PreCommit\Domain\Parser
     */
    protected $parser;

    public function __construct(Executor $executor, Parser $parser)
    {
        $this->executor = $executor;
        $this->parser   = $parser;
    }

    public function runOne(Runner $app, Request $request)
    {
        $file   = $request->get('file');
        $result = $this->executor->run($file, 'coredev');

        return new JsonResponse($this->parser->parse($file, $result));
    }
} 