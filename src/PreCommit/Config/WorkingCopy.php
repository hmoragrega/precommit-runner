<?php

namespace PreCommit\Config;

use Silex;
use PreCommit;

class WorkingCopy implements \Silex\ServiceProviderInterface
{
    /**
     * Initial configuration of the app for working copies.
     *
     * @param Silex\Application $runner
     */
    public static function configure(Silex\Application $runner)
    {
        $config = new self;
        $config->register($runner);
    }

    /**
     * Registers services on the given app.
     *
     * @param Silex\Application $app An Application instance
     */
    public function register(Silex\Application $app)
    {
        $app['debug']        = true;
        $app['user']         = $_SERVER['WC_OWNER'];
        $app['domain']       = $_SERVER['INSTANCE_DOMAIN'];
        $app['base']         = str_replace('/home/', '/mnt/', $_SERVER['DOCUMENT_ROOT']);

        // Paths
        $app['path.base']      = realpath( __DIR__ . '/../../../' ) . '/';
        $app['path.source']    = $app['path.base'] . 'src/';
        $app['path.templates'] = $app['path.base'] . 'templates/';
        $app['path.instances'] = $app['base'] . 'core3dev/instances';
        $app['path.tests']     = $app['base'] . 'core3dev/test/PHPUnit/instances';
        $app['path.projects']  = $app['base'] . 'projects';

        $app['file.blacklist'] = '#templates|inherited|_inheritance|generated|base\.config|i18n#';

        $this->registerRoutes($app);
        $this->registerModels($app);
        $this->registerServices($app);
        $this->registerController($app);
    }

    protected function registerServices(Silex\Application $app)
    {
        $app->register(new Silex\Provider\TwigServiceProvider(), array('twig.path' => $app['path.templates']));
        $app->register(new Silex\Provider\UrlGeneratorServiceProvider());
        $app->register(new Silex\Provider\ServiceControllerServiceProvider());
    }

    protected function registerModels(Silex\Application $app)
    {
        $app['instance.lister'] = $app->share(function($app) {
                return new PreCommit\Domain\Instance\Lister($app['path.instances'], $app['path.projects']);
            });
        $app['file.lister'] = $app->share(function($app) {
                return new PreCommit\Domain\File\Lister(
                    $app['file.blacklist'],
                    $app['path.instances'],
                    $app['path.tests'],
                    $app['path.projects']
                );
            });
        $app['executor'] = $app->share(function($app) {
                return new PreCommit\Domain\Executor($app['base'], $app['user']);
            });
        $app['parser'] = $app->share(function() {
                return new PreCommit\Domain\Parser();
            });
    }

    protected function registerController(Silex\Application $app)
    {
        $app['home.controller'] = $app->share(function($app) {
                return new PreCommit\Controller\Home($app['instance.lister'], $app['file.lister'], $app['twig']);
            });
        $app['run.controller'] = $app->share(function($app) {
                return new PreCommit\Controller\Run($app['executor'], $app['parser']);
            });
    }

    protected function registerRoutes(Silex\Application $app)
    {
        $app->get('/', 'home.controller:index');
        $app->post('/run', 'run.controller:runOne');
    }

    /**
     * Bootstraps the application.
     */
    public function boot(Silex\Application $app)
    {
    }
}