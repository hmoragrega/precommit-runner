<?php
/**
 * Created by PhpStorm.
 * User: hilari.moragrega
 * Date: 8/10/14
 * Time: 9:35
 */

namespace PreCommit\Domain;

class Parser
{
    const OK_REGEX = '/\nOK\n/';
    const ERROR_REGEX = '/(\d+) errors, (\d+) warnings/';

    public function parse($file, $result)
    {
        $return = array(
            'hash'     => md5($file),
            'result'   => false,
            'output'   => nl2br($result),
            'errors'   => 0,
            'warnings' => 0,
        );

        if (preg_match(self::ERROR_REGEX, $result, $matches)) {
            $return['errors']   = intval($matches[1]);
            $return['warnings'] = intval($matches[2]);
        } else {
            $return['result'] = preg_match(self::OK_REGEX, $result);
        }

        return $return;
    }
}