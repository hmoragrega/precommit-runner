<?php
/**
 * Created by PhpStorm.
 * User: hilari.moragrega
 * Date: 8/10/14
 * Time: 9:35
 */

namespace PreCommit\Domain;

class Executor
{
    protected $baseDir;
    protected $user;

    public function __construct($baseDir, $user)
    {
        $this->baseDir = $baseDir;
        $this->user    = $user;
    }

    public function run($file, $environment)
    {
        $command = sprintf(
            'php %slibs/hooks/server/pre-commit-local.php %s %s %s',
            $this->baseDir,
            $file,
            $this->user,
            $environment
        );

        return shell_exec( escapeshellcmd( $command ) );
    }

    public function runAsync($file, $environment)
    {
        $command = sprintf(
            'php %sutils/precommit-runner/run.php "%s" "%s" > /dev/null 2>/dev/null &',
            $this->baseDir,
            $file,
            $environment
        );

        return shell_exec( $command );
    }
}