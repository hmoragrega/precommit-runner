<?php

namespace PreCommit\Domain\File;

use RuntimeException;

class Lister
{
    const PHP_FILE_EXTENSION = 'php';

    protected $instancesPath;
    protected $testsPath;
    protected $projectsPath;
    protected $blacklist;
    protected $files = array(
        'config'  => array(
            'config'     => array(),
        ),
        'code'    => array(
            'Model'      => array(),
            'Controller' => array(),
        ),
        'tests'   => array(
            'Model'      => array(),
            'Controller' => array(),
        ),
    );

    public function __construct($blacklist, $instancesPath, $testsPath, $projectsPath)
    {
        if (!is_dir($instancesPath) || !is_readable($instancesPath)) {
            throw new RuntimeException("Instances path is invalid");
        }

        if (!is_dir($testsPath) || !is_readable($testsPath)) {
            throw new RuntimeException("Instance tests path is invalid");
        }

        if (is_dir($projectsPath) && is_readable($projectsPath)) {
            $this->projectsPath  = rtrim($projectsPath, '/') . '/';
        }

        $this->blacklist     = $blacklist;
        $this->instancesPath = rtrim($instancesPath, '/') . '/';
        $this->testsPath     = rtrim($testsPath, '/') . '/';
    }

    /**
     * Tries to returns the valid path for subversion instances or git projects
     */
    protected function getValidPathsByInstanceType($instance)
    {
        // Try subversion instance
        $instancePath = $this->instancesPath . $instance . '/';
        if (is_dir($instancePath)) {
            return array(
                'code'   => $instancePath,
                'tests'  => $this->testsPath . $instance . '/',
                'config' => $instancePath . 'config/',
            );
        }

        // If not try, git projects path
        if ($this->projectsPath) {
            $projectPath = $this->projectsPath . $instance . '/src/';
            if (is_dir($projectPath)) {
                return array(
                    'code'   => $projectPath,
                    /* fingers crossed below */
                    'tests'  => $this->projectsPath . $instance . '/tests/',
                    'config' => $this->projectsPath . $instance . '/resources/config/',
                );
            }
        }

        throw new \Exception("Could not found a valid path for the $instance instance");
    }

    /**
     * Returns a list of files, by package (code, tests and config) and subpackage (First namespace level)
     */
    public function getFiles($instance)
    {
        if (empty($instance)) {
            return array();
        }

        $paths = $this->getValidPathsByInstanceType($instance);

        $this->findFiles($paths['code'], $this->files['code']);
        $this->findFiles($paths['tests'], $this->files['tests']);
        $this->findFiles($paths['config'], $this->files['config'], 'config');

        return $this->files;
    }

    /**
     * Recursive shit that builds a holy fuck... but works :S
     */
    protected function findFiles($dir, array &$files, $package = null, $subpackage = '- Root -', $namespace = null)
    {
        if (!is_dir($dir)) {
            return;
        }

        $dirs = array();

        // Clean the scanned dir.
        $scanned_dir = array_diff(scandir($dir), array('..', '.'));

        foreach ($scanned_dir as $path) {
            $fullPath = $dir . $path;

            // Check if we're collecting package files (basically config, code or test)
            if ($package) {
                // If it is a valid file we store it
                if ($this->validFile($fullPath)) {
                    $files[$package][$subpackage][$fullPath] = array(
                        'path' => $namespace . $path,
                        'hash' => md5($fullPath)
                    );

                // Subdirectory, save it for later to avoid subpackage problems
                } elseif (is_dir($fullPath)) {
                    $dirs[$fullPath] = $path;
                }
            }

            // Find for a package directory
            elseif (is_dir($fullPath)) {
                if (isset($files[$path])) {
                    // Found a package directory, begin collecting files
                    $this->findFiles($fullPath . '/', $files, $path);
                } else {
                    // Go down, see how far we get...
                    $this->findFiles($fullPath . '/', $files);
                }
            }
        }

        // Get files for the subdirectories
        foreach ($dirs as $fullPath => $path) {
            $subpackage = null == $namespace ? $path : $subpackage;
            $this->findFiles($fullPath . '/', $files, $package, $subpackage, $namespace . $path . '/');
        }
    }

    protected function validFile($path)
    {
        return is_file($path)
        && pathinfo($path, PATHINFO_EXTENSION) == self::PHP_FILE_EXTENSION
        && !preg_match($this->blacklist, $path);
    }
} 