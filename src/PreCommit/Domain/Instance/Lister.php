<?php

namespace PreCommit\Domain\Instance;

use PreCommit\Domain\Instance;
use RuntimeException;

class Lister
{
    protected $instancesPath;
    protected $projectsPath;

    public function __construct($instancesPath, $projectsPath)
    {
        if (!is_dir($instancesPath) || !is_readable($instancesPath)) {
            throw new RuntimeException("Instances path is invalid");
        }

        if (is_dir($projectsPath) && is_readable($projectsPath)) {
            $this->projectsPath = $projectsPath;
        }

        $this->instancesPath = $instancesPath;
    }

    public function getAll()
    {
        $instances = array(
            'subversion' => array(),
            'git'        => array(),
        );

        foreach (scandir($this->instancesPath) as $instance) {
            if (strpos($instance, '.') !== 0) {
                $instances['subversion'][] = $instance;
            }
        }

        if ($this->projectsPath) {
            foreach (scandir($this->projectsPath) as $project) {
                if (strpos($project, '.') !== 0) {
                    $instances['git'][] = $project;
                }
            }
        }

        return $instances;
    }
} 